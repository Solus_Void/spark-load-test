package de.edatasystems.sparkloadtest.writer;

import de.edatasystems.sparkloadtest.queue.ESparkQueue;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.DependsOn;

@Configuration
@Log4j2
public class SparkQueueConsumerInit {

    @Value("${spark.batch.size:10000}")
    private int sparkBatchSize;

    @Bean
    @DependsOn(value = {"spark_conf", "spark_sc"})
    public void init() {
        ESparkQueue.INSTANCE.setSparkBatchSize(sparkBatchSize);
        log.info("Attempting to start spark queue consumer thread with batch size: {}.", sparkBatchSize);
        new Thread(new SparkQueueConsumer()).start();
        log.info("Successfully started spark queue consumer thread.");
    }
}