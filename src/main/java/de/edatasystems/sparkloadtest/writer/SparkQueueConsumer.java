package de.edatasystems.sparkloadtest.writer;

import de.edatasystems.sparkloadtest.queue.ESparkQueue;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

@Slf4j
public class SparkQueueConsumer implements Runnable {

    @Value("${spark.queue.consumer.sleep.duration:1000}")
    private long sleepDuration;

    @Override
    public void run() {
        try {
            while (true) {
                if (ESparkQueue.INSTANCE.getSparkQueue().isEmpty() && ESparkQueue.INSTANCE.optimizedQueueIsEmpty()) {
                    Thread.sleep(sleepDuration);
                } else {
                    ESparkQueue.INSTANCE.writeData();
                }
            }
        } catch (InterruptedException e) {
            log.error("Attempting to interrupt current Thread");
            Thread.currentThread().interrupt();
        }
    }
}

