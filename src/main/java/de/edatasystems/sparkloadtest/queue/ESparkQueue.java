package de.edatasystems.sparkloadtest.queue;

import de.edatasystems.sparkloadtest.domain.sparkwriter.SparkDataConfig;
import lombok.extern.slf4j.Slf4j;
import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Encoder;
import org.apache.spark.sql.Encoders;
import org.apache.spark.sql.SparkSession;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

@Slf4j
public enum ESparkQueue {

    INSTANCE(new LinkedBlockingQueue<>(), new LinkedBlockingQueue<>(), SparkSession.builder().getOrCreate());

    private final BlockingQueue<SparkDataConfig> sparkQueue;
    private final BlockingQueue<SparkDataConfig> optimizedSparkQueue;
    private final SparkSession sparkSession;

    private int sparkBatchSize = 10000;

    ESparkQueue(BlockingQueue<SparkDataConfig> sparkQueue, BlockingQueue<SparkDataConfig> optimizedSparkQueue, SparkSession sparkSession) {
        this.sparkQueue = sparkQueue;
        this.optimizedSparkQueue = optimizedSparkQueue;
        this.sparkSession = sparkSession;
    }

    public SparkSession getSparkSession() {
        return sparkSession;
    }

    public BlockingQueue<SparkDataConfig> getSparkQueue() {
        return sparkQueue;
    }

    public boolean optimizedQueueIsEmpty() {
        return optimizedSparkQueue.isEmpty();
    }

    public int getSparkBatchSize() {
        return sparkBatchSize;
    }

    public void setSparkBatchSize(int sparkBatchSize) {
        this.sparkBatchSize = sparkBatchSize;
    }

    public void writeData() {

        optimizeQueue();

        SparkDataConfig sparkDataConfig = optimizedSparkQueue.poll();

        Encoder<Object> sparkTestDataEncoder = Encoders.bean(sparkDataConfig.getSparkEncoderClass());

        Dataset<Object> dataset = sparkSession.createDataset(sparkDataConfig.getData(), sparkTestDataEncoder);

        if (sparkDataConfig.getSparkPartitionBy() != null && !sparkDataConfig.getSparkPartitionBy().isEmpty()) {
            dataset.write().mode(sparkDataConfig.getSparkSaveMode()).options(sparkDataConfig.getSparkOptions())
                    .partitionBy(sparkDataConfig.getSparkPartitionBy())
                    .json(sparkDataConfig.getSparkOutputPath());
        } else {
            dataset.write().mode(sparkDataConfig.getSparkSaveMode()).options(sparkDataConfig.getSparkOptions())
                    .json(sparkDataConfig.getSparkOutputPath());
        }
        log.info("Finished writing data for entity: {}, with {} entries.",
                sparkDataConfig.getSparkEncoderClass(), sparkDataConfig.getData().size());
    }

    public void optimizeQueue() {

        List<SparkDataConfig> sparkDataConfigList = new ArrayList<>();
        Map<SparkDataConfig, List<Object>> sparkDataConfigMap = new HashMap<>();

        sparkQueue.drainTo(sparkDataConfigList, sparkBatchSize);

        for (SparkDataConfig sparkDataConfig : sparkDataConfigList) {
            if (!sparkDataConfigMap.containsKey(sparkDataConfig)) {
                sparkDataConfigMap.put(sparkDataConfig, new ArrayList<>());
            }
            sparkDataConfigMap.get(sparkDataConfig).addAll(sparkDataConfig.getData());
        }

        for (Map.Entry<SparkDataConfig, List<Object>> entry : sparkDataConfigMap.entrySet()) {
            entry.getKey().setData(entry.getValue());

            optimizedSparkQueue.offer(entry.getKey());
        }
    }
}
