package de.edatasystems.sparkloadtest.domain;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class SparkTestData {
    private Long id;
    private String name;
    private String description;
    private Long group;
    private List<String> stringList;

    public SparkTestData(Long id, String name, String description, Long group, Long stringListLength) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.group = group;
        stringList = new ArrayList<>();
        for (long i = 0; i < stringListLength; i++) {
            stringList.add("Index: " + i);
        }
    }
}
