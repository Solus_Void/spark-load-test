package de.edatasystems.sparkloadtest.domain.sparkwriter;

import lombok.Data;
import org.apache.spark.sql.SaveMode;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@Data
public class SparkDataConfig {
    /* required parameters. therefore in constructor parameters */
    private String sparkOutputPath;
    private Class sparkEncoderClass;
    private List<Object> data;

    /* optional parameters. therefore customizable via setter methods */
    private String sparkPartitionBy;
    private SaveMode sparkSaveMode; //default value: SaveMode.Append
    private Map<String, String> sparkOptions; // default value: ("nullValue", null)

    public SparkDataConfig(String sparkOutputPath, Class sparkEncoderClass, List<Object> data) {
        this.sparkOutputPath = sparkOutputPath;
        this.sparkEncoderClass = sparkEncoderClass;
        this.data = data;
        this.sparkSaveMode = SaveMode.Append;

        /* always add this to avoid issues with null values */
        sparkOptions = new HashMap<>();
        sparkOptions.put("nullValue", null);
    }

    public SaveMode getSparkSaveMode() {
        return sparkSaveMode;
    }

    /* custom setter so the default option does not get removed by accident.
    The only way to remove the default option is to explicitly do so by calling remove */
    public void setSparkOptions(Map<String, String> sparkOptions) {
        this.sparkOptions.putAll(sparkOptions);

    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SparkDataConfig)) return false;
        SparkDataConfig that = (SparkDataConfig) o;
        return getSparkOutputPath().equals(that.getSparkOutputPath()) && getSparkEncoderClass().equals(that.getSparkEncoderClass()) && Objects.equals(getSparkPartitionBy(), that.getSparkPartitionBy()) && getSparkSaveMode() == that.getSparkSaveMode() && Objects.equals(getSparkOptions(), that.getSparkOptions());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSparkOutputPath(), getSparkEncoderClass(), getSparkPartitionBy(), getSparkSaveMode(), getSparkOptions());
    }
}