package de.edatasystems.sparkloadtest.domain;

import lombok.Data;

@Data
public class SparkTestData2 {
    private Long id;
    private String name;
    private String description;
    private Long group;
    private String uuid;

    public SparkTestData2(Long id, String name, String description, Long group, String uuid) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.group = group;
        this.uuid = uuid;
    }
}
