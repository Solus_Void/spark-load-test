package de.edatasystems.sparkloadtest.controller;

import de.edatasystems.sparkloadtest.domain.SparkTestData;
import de.edatasystems.sparkloadtest.domain.SparkTestData2;
import de.edatasystems.sparkloadtest.domain.sparkwriter.SparkDataConfig;
import de.edatasystems.sparkloadtest.queue.ESparkQueue;
import de.edatasystems.sparkloadtest.rest.ISparkController;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

@Service
public class SparkController implements ISparkController {

    @Override
    public void provokeException(long listSize, long amountOfLists, long stringListSize) {

        List<List<SparkTestData>> testDataList = new LinkedList<>();

        for (long i = 0; i < amountOfLists; i++) {
            List<SparkTestData> sparkTestDataList = new LinkedList<>();
            for (long j = 0; j < listSize; j++) {
                sparkTestDataList
                        .add(new SparkTestData(i, "name_" + j, "datalist_" + i + ". Entry: " + j, i, stringListSize));
            }
            testDataList.add(sparkTestDataList);
        }

        for (List<SparkTestData> sparkTestDataList : testDataList) {
            SparkDataConfig sparkDataConfig = new SparkDataConfig("./spark-test/v1"
                    , SparkTestData.class
                    , new ArrayList<>(sparkTestDataList));
            sparkDataConfig.setSparkPartitionBy("group");
            ESparkQueue.INSTANCE.getSparkQueue().offer(sparkDataConfig);
        }
    }

    @Override
    public void provokeExceptionV2(long listSize, long amountOfLists) {
        List<List<SparkTestData2>> testDataList2 = new LinkedList<>();

        for (long i = 0; i < amountOfLists; i++) {
            List<SparkTestData2> sparkTestDataList2 = new LinkedList<>();
            for (long j = 0; j < listSize; j++) {
                sparkTestDataList2
                        .add(new SparkTestData2(i, "name_" + j, "datalist_" + i + ". Entry: " + j, i, UUID.randomUUID().toString()));
            }
            testDataList2.add(sparkTestDataList2);
        }

        for (List<SparkTestData2> sparkTestDataList2 : testDataList2) {
            SparkDataConfig sparkDataConfig2 = new SparkDataConfig("./spark-test/v2"
                    , SparkTestData2.class
                    , new ArrayList<>(sparkTestDataList2));
            sparkDataConfig2.setSparkPartitionBy("group");
            ESparkQueue.INSTANCE.getSparkQueue().offer(sparkDataConfig2);
        }
    }
}

